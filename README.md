# Диплом магистра
Это проект моей магистерской диссертации на тему "Разработка и реализация модели поведения неигровых персонажей с применением нечеткой логики". Вы также можете ознакомиться с демонстрационными видео по [ссылке](https://drive.google.com/drive/folders/1qCNdAnE1bmf9CWyxxUaw1V2h7mlY19L3?usp=sharing)

# MagistrDiplom
This is the project of my master's thesis on the topic "Development and implementation of a model of behavior of non-player characters using fuzzy logic". You can also view the demo videos at the [link](https://drive.google.com/drive/folders/1qCNdAnE1bmf9CWyxxUaw1V2h7mlY19L3?usp=sharing)


